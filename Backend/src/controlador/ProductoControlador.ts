import ProductoDAO from '../dao/ProductoDao';
import { Request, Response } from 'express';
import ProductoDao from '../dao/ProductoDao';

class ProductoControlador extends ProductoDao {

    public consulta(req: Request, res: Response): void {
        ProductoControlador.obtenerProductos(res);
    }

    public crear(req: Request, res: Response): void {
        ProductoControlador.crearProducto(req.body, res);
    }

    public eliminar(req: Request, res: Response): void {
        ProductoControlador.eliminarProducto(req.params.codigo, res);
    }

    public actualizar(req: Request, res: Response): void {
        ProductoControlador.actualizarProducto(req.params.codigo, req.body, res);
    }

    public consultaUno(req: Request, res: Response): void {
        ProductoControlador.obtenerUnProducto(req.params.codigo, res);
    }

};

const productoControlador = new ProductoControlador();
export default productoControlador;