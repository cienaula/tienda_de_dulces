import { Response } from 'express';
import ProductoEsquema from "../esquema/ProductoEsquema";
import UsuarioEsquema from '../esquema/UsuarioEsquema';

class ProductoDao {

    // Consultar los datos de un Producto por un código específico
    // ************************************************************************************
    protected static async obtenerUnProducto(identificador: any, res: Response): Promise<any> {
        const jsonProducto = { _id: identificador };
        const existeProducto = await ProductoEsquema.findOne(jsonProducto).exec();
        if (existeProducto) {
            res.status(200).json(existeProducto);
        } else {
            res.status(400).json({ respuesta: "El Producto NO existe con ese identificador" });
        }
    }
    // ************************************************************************************


    // Obtener Productos en orden descendente (-1)
    // ************************************************************************************
    protected static async obtenerProductos(res: Response): Promise<any> {
        const datos = await ProductoEsquema.find().sort({ _id: -1 });;
        res.status(200).json(datos);
    }
    // ************************************************************************************


    // Crear Producto verificando su existencia
    // ************************************************************************************
    protected static async crearProducto(parametros: any, res: Response): Promise<any> {
        delete parametros._id;
        delete parametros.datosUsuario;
        const existe = await ProductoEsquema.findOne(parametros);

        if (existe) {
            res.status(400).json({ respuesta: "El Producto ya existe" });
        } else {
            const objProducto = new ProductoEsquema(parametros);
            objProducto.save((miError, objeto) => {
                if (miError) {
                    res.status(400).json({ respuesta: 'Error al crear el Producto' });
                } else {
                    res.status(200).json({ id: objeto._id });
                }
            });
        }
    }
    // ************************************************************************************


    // Eliminar Producto por código, verificando antes que no tenga usuarios asociados
    // ************************************************************************************
    protected static async eliminarProducto(parametro: any, res: Response): Promise<any> {
        const llave = { _id: parametro };
        const cantidad = await UsuarioEsquema.countDocuments({ codProducto: llave });
        if (cantidad > 0) {
            res.status(400).json({ respuesta: 'Error, el Producto tiene usuarios relacionados' });
        } else {
            const existe = await ProductoEsquema.findById(parametro).exec();
            if (existe) {
                ProductoEsquema.deleteOne({ _id: parametro }, (miError: any, objeto: any) => {
                    //ProductoEsquema.findByIdAndDelete(parametro, (miError: any, objeto: any) => {
                    if (miError) {
                        res.status(400).json({ respuesta: 'Error al eliminar el Producto' });
                    } else {
                        res.status(200).json({ eliminado: objeto });
                    }
                });
            } else {
                res.status(400).json({ respuesta: "El Producto NO existe" });
            }
        }
    }
    // ************************************************************************************


    // Actualizar Producto por código y con body JSON
    // ************************************************************************************
    protected static async actualizarProducto(codigo: string, parametros: any, res: Response): Promise<any> {
        const existe = await ProductoEsquema.findById(codigo).exec();
        if (existe) {
            ProductoEsquema.findByIdAndUpdate(
                { _id: codigo },
                { $set: parametros },
                (miError: any, objeto: any) => {
                    if (miError) {
                        res.status(400).json({ respuesta: 'Error al actualizar el Producto' });
                    } else {
                        res.status(200).json({ antiguo: objeto, nuevo: parametros });
                    }
                });
        } else {
            res.status(400).json({ respuesta: "El Producto NO existe" });
        }
    }
    // ************************************************************************************

};

export default ProductoDao;