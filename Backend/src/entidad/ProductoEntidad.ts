export class ProductoEntidad {
    public nombreProducto: string;
    public estadoProducto: number;
  
    constructor(nomp: string, esta: number) {
      this.nombreProducto = nomp;
      this.estadoProducto = esta;
    }
  }
  
  export default ProductoEntidad;  