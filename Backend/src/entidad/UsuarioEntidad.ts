import PerfilEntidad from './PerfilEntidad';
export class UsuarioEntidad {
    public nombreUsuario: string;
    public correoUsuario: string;
    public claveUsuario: string;
    public fechaCreacionUsuario: Date;
    public estadoUsuario: number;
    public avatarUsuario: string;
    public codPerfil: PerfilEntidad;
  
    constructor(nomp: string, esta: number, ava: string,
      corr: string, clave: string, fecha: Date, cope: PerfilEntidad) {
      this.nombreUsuario = nomp;
      this.correoUsuario = corr;
      this.claveUsuario = clave
      this.fechaCreacionUsuario = fecha;
      this.estadoUsuario = esta;
      this.avatarUsuario = ava;
      this.codPerfil = cope;
      
    }
  }
  
  export default UsuarioEntidad;  