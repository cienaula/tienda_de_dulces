import {model, Schema} from "mongoose";
import ProductoEntidad from "../entidad/ProductoEntidad";

const ProductoEsquema = new Schema<ProductoEntidad>({
    nombreProducto:{type:String, required:true, unique:true, trim:true},
    estadoProducto:{type:Number, enum:[1,2,3], default:1}
},{versionKey:false});

export default model("Producto", ProductoEsquema, "Producto");