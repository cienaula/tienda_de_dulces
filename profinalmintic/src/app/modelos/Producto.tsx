class Producto {
    public _id: string;
    public nombreProducto: string;
    public idCategoria: string;
    public cantidadProducto: number;
    public estadoProducto: number;
  
    constructor(id: string, idCa:string, nom: string, cant:number, est: number) {
      this._id = id;
      this.nombreProducto = nom;
      this.idCategoria = idCa;
      this.cantidadProducto = cant;
      this.estadoProducto = est;
    }
  }
  
  export default Producto;  